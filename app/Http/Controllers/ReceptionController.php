<?php

namespace App\Http\Controllers;

use App\Models\Card;
use App\Models\CardGymLog;
use App\Models\Gym;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class ReceptionController extends Controller
{
    /**
     * @param Request $request
     * @param string $objectId
     * @param string $cardId
     * @return JsonResponse
     */
    public function process(Request $request, string $objectId, string $cardId): JsonResponse
    {
        $gym = Gym::find($objectId);
        $card = Card::find($cardId);

        if (empty($gym) || empty($card)) {
            return response()->json('Invalid request', 400);
        }

        $validation = $this->validateRequest($gym, $card);
        if (!$validation['isValid']) {
            return response()->json($validation['message'], $validation['status']);
        }
        $log = CardGymLog::create([
            'id' => Str::uuid()->toString(),
            'gym_id' => $gym->id,
            'card_id' => $card->id,
            'created_at' => new \DateTime()
        ]);
        return response()->json([
            'object_name' => $log->gym->name,
            'first_name' => $log->card->user->first_name,
            'last_name' => $log->card->user->last_name
        ], 201);
    }

    /**
     * @param object $gym
     * @param object $card
     * @return array|bool[]
     */
    private function validateRequest(object $gym, object $card): array
    {

        if ($this->isCardExpired($card->valid_until)) {
            return [
                'isValid' => false,
                'message' => 'Card has expired',
                'status' => 200
            ];
        }

        if ($this->hasUserAlreadyVisitedGym($gym, $card)) {
            return [
                'isValid' => false,
                'message' => 'The card is already used for this gym today',
                'status' => 200
            ];
        }

        return ['isValid' => true];
    }

    /**
     * @param string $cardValidationDate
     * @return bool
     */
    private function isCardExpired(string $cardValidationDate): bool
    {
        $currentDate = (new \DateTime())->format('Y-m-d H:i:s');
        return $cardValidationDate < $currentDate;
    }

    /**
     * @param object $gym
     * @param object $card
     * @return bool
     */
    private function hasUserAlreadyVisitedGym(object $gym, object $card): bool
    {
        $log = CardGymLog::where('card_id', $card->id)
            ->where('gym_id', $gym->id)
            ->whereDate('created_at', Carbon::today())
            ->first();

        return !empty($log);
    }

}
