<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @method static find(string $cardId)
 */
class Card extends Model
{
    use HasFactory, Uuids;

    public $timestamps = false;

    /**
     * @var string[]
     */
    protected $fillable = ['serial', 'valid_until'];


    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function logs(): HasMany
    {
        return $this->hasMany(CardGymLog::class);
    }

}
