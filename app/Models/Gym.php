<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @method static find(string $objectId)
 */
class Gym extends Model
{
    use HasFactory, Uuids;

    public $timestamps = false;

    /**
     * @var string[]
     */
    protected $fillable = ['name', 'address'];

    public function logs(): HasMany
    {
        return $this->hasMany(CardGymLog::class);
    }
}
