<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class GymFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->randomElement(['Super', 'Sport', 'Ultra', 'Titan', 'Shark', 'Extra', 'Top fit']) . ' ' . 'gym',
            'address' => $this->faker->address
        ];
    }
}
