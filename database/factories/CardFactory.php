<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class CardFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $user = User::inRandomOrder()->first();
        $daysInFuture = rand(2, 30);
        return [
            'valid_until' => (new \DateTime())->modify("+ {$daysInFuture} day")->format("Y-m-d H:i:s"),
            'user_id' => $user->id
        ];
    }
}
