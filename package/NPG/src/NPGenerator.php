<?php

namespace Naskovic\NPG;


class NPGenerator
{
    private $passwordString = "abcdefghijklmnopqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ123456789";
    private $symbols = '!#$%&(){}[]=';

    /**
     * @param int $length
     * @param int $strength
     * @return string
     */
    public function Generate($length = 6, $strength = 1): string
    {
        $password = '';
        if ($strength === 1) {
            $password = $this->generatePrimaryStrengthString();
        } else if ($strength === 2) {
            $password = $this->generateSecondaryStrengthString();
        } else if ($strength === 3) {
            $password = $this->generateThirdStrengthString();
        }

        $stringFromStrengthLength = strlen($password);
        if ($stringFromStrengthLength >= $length) {
            return $password;
        }
        $stringFromLength = $this->generatePasswordFromLength($length - $stringFromStrengthLength);
        $password = $password . $stringFromLength;
        return str_shuffle($password);
    }

    /**
     * @return string
     */
    private function generatePrimaryStrengthString(): string
    {
        $resource = substr(str_shuffle($this->passwordString), 0, 3);
        $data = strtoupper($resource);
        return lcfirst($data);
    }

    /**
     * @return string
     */
    private function generateSecondaryStrengthString(): string
    {
        $resource = $this->generatePrimaryStrengthString();
        $numbers = [2, 3, 4, 5];
        shuffle($numbers);
        $data = strtoupper($resource) . $numbers[0];
        return lcfirst($data);
    }

    /**
     * @return string
     */
    private function generateThirdStrengthString(): string
    {
        $symbols = str_shuffle($this->symbols);
        return substr($symbols, rand(0, strlen($symbols) - 1), rand(1, 5));
    }

    /**
     * @param $length
     * @return string
     */
    private function generatePasswordFromLength($length): string
    {
        if ($length <= 1) {
            return '';
        }
        $data = str_shuffle($this->passwordString);
        return substr($data, 0, $length - 1);
    }
}
