<?php

namespace Naskovic\NPG;

use Illuminate\Support\ServiceProvider;

class NPGServiceProvider extends ServiceProvider
{
    public function boot()
    {
    }

    public function register()
    {
        $this->app->singleton('npgenerator', function ($app) {
            return new NPGenerator();
        });
    }

    public function provides()
    {
        return ['npgenerator'];
    }
}
