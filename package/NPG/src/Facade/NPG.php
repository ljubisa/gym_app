<?php

namespace Naskovic\NPG\Facade;

use Illuminate\Support\Facades\Facade;

class NPG extends Facade
{

    /**
     * Get the binding in the IoC container.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'npgenerator';
    }
}
